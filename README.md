# aws-kill-switch

## Overview

This tool is a proverbial kill switch for AWS IAM and VPC access.

#### Shunning

When run with an action of `shun`, this script will:

1. Deny all traffic to the intended VPC(s), through the creation or replacement of VPC NACLs in the first position.

    ![](./static/acl_deny.png)

2. Remove all IAM users' console access and deactivate all active accesskeys, with the exception of the IAM user and accesskey with which it was run.

    _Note: this step is not optional, and will be performed on each `shun` run._

3. Gracefully shutdown all running EC2 instances.

    _Note: this step is not optional, and will be performed on each `shun` run._

#### Unshunning

When run with an action of `unshun`, this script will:

1. Remove existing VPC NACLs that deny all traffic in the first position of the intended VPC(s).

_Note: Starting EC2 instances and IAM unshunning, that is enabling users and activating accesskeys, will be a manual process._

## Dependencies

This script requires the [AWS Command Line Interface](https://docs.aws.amazon.com/cli/).

Installation instructions can be found here: [Installing the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)

## Usage

```shell
Usage: aws-kill-switch.sh -p [profile] -r [region|ALL] -a [shun|unshun] -v [vpcid|ALL]
--------------------------
OPTIONS:
 -p    awscli profile
 -r    AWS Region or ALL
 -v    VPC ID or ALL
 -a    action to take on VPC [shun|unshun]
 -h    show this message
```

## Examples:

#### Shun all traffic for `vpc-a1s2d3f4` in the `us-east-1` region:

```shell
#> ./aws-kill-switch.sh -p dw-dmpc-scranton -v vpc-a1s2d3f4 -a shun -r us-east-1
+-----------------------------------+
  About to shun access for...
+-----------------------------------+
  Account: [dmpc-scranton]
   Region: [us-east-1]
      VPC: [vpc-a1s2d3f4]
+-----------------------------------+

ARE YOU SURE? [y/N] y
Shunned [vpc-a1s2d3f4 - (acl-f4d3s2a1)]
Shunning users and disabling accesskeys...
- Removed login profile [andy_bernard]
- Removed access key [halpert_api : AKIAJBW7LQNIEYN3DUWQ]
- No login profile exists [halpert_api]
- Removed login profile [stanley_hudson]
Shunned all users, except [dwight_schrute], and disabled access (killswitch-20170505_120000.log)
```

#### Shun all traffic for `ALL` VPCs in `ALL` regions:

```shell
#> ./aws-kill-switch.sh -p dw-dmpc-scranton -v ALL -a shun -r ALL
+-----------------------------------+
  About to shun access for...
+-----------------------------------+
  Account: [dmpc-scranton]
   Region: [ALL]
      VPC: [ALL]
+-----------------------------------+

ARE YOU SURE? [y/N] y
Shunned [vpc-a1s2d3f1 - (acl-f4d3s2a0)]
Shunned [vpc-a1s2d3f2 - (acl-f4d3s2a1)]
Shunned [vpc-a1s2d3f3 - (acl-f4d3s2a2)]
Shunned [vpc-a1s2d3f4 - (acl-f4d3s2a3)]
Shunned [vpc-a1s2d3f5 - (acl-f4d3s2a4)]
Shunned [vpc-a1s2d3f6 - (acl-f4d3s2a5)]
Shunned [vpc-a1s2d3f7 - (acl-f4d3s2a6)]
Shunned [vpc-a1s2d3f8 - (acl-f4d3s2a7)]
Shunned [vpc-a1s2d3f9 - (acl-f4d3s2a8)]
Shunned [vpc-b1s2d3f0 - (acl-f4d3s2a9)]
Shunned [vpc-b1s2d3f1 - (acl-f4d3s2b0)]
Shunned [vpc-b1s2d3f2 - (acl-f4d3s2b1)]
Shunned [vpc-b1s2d3f3 - (acl-f4d3s2b2)]
Shunned [vpc-b1s2d3f4 - (acl-f4d3s2b3)]
Shunning users and disabling accesskeys...
- Removed login profile [andy_bernard]
- Removed access key [halpert_api : AKIAJBW7LQNIEYN3DUWQ]
- No login profile exists [halpert_api]
- Removed login profile [stanley_hudson]
Shunned all users, except [dwight_schrute], and disabled access (killswitch-20170505_120000.log)
```

#### Unshun all traffic for `vpc-a1s2d3f4` in the `us-east-1` region:

```shell
#> ./aws-kill-switch.sh -p dw-dmpc-scranton -v vpc-a1s2d3f4 -a unshun -r us-east-1
+-----------------------------------+
  About to unshun access for...
+-----------------------------------+
  Account: [dmpc-scranton]
   Region: [us-east-1]
      VPC: [vpc-a1s2d3f4]
+-----------------------------------+

ARE YOU SURE? [y/N] y
Unshunned [vpc-a1s2d3f4 - (acl-f4d3s2a1)]
Unshunning users and accesskeys is a manual process, please refer to the shunning output file for affected users.
```
