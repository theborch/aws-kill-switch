#!/bin/bash
TIMESTAMP=$(date -d now +%Y%m%d)

usage() {
cat << EOF
Usage: `basename $0` -p [profile] -r [region|ALL] -a [shun|unshun] -v [vpcid|ALL]
--------------------------
OPTIONS:
 -p    awscli profile
 -r    AWS Region or ALL
 -v    VPC ID or ALL
 -a    action to take on VPC [shun|unshun]
 -h    show this message
EOF
}

accesskeys() {
  local KEYS=$(aws iam list-access-keys --user-name $1 --query "AccessKeyMetadata[*].[Status,AccessKeyId]" --output text | awk '/^Active/ {print $2}')
  echo ${KEYS}
}

account() {
  local ACCOUNT_ID=$(aws sts get-caller-identity --output text --query 'Account' 2>&1 | awk '!/^$/')
  if [[ ${ACCOUNT_ID} =~ [0-9]+ ]]; then
    ACCOUNT=$(aws iam list-account-aliases --query "AccountAliases" --output text)
  else
    echo "ERROR: No account accessible - [${ACCOUNT_ID}]"
    exit 1
  fi
}

aclid() {
  local ACLID=$(aws ec2 describe-network-acls --filters "Name=vpc-id,Values=$1" --query "NetworkAcls[*].NetworkAclId" --output text)
  if [[ -z "${ACLID}" && ${REGION} != "ALL" ]]; then
    echo "`basename $0`: Invalid vpc-id: [$1]"
    exit 1
  else
    echo ${ACLID}
  fi
}

confirm() {
cat << EOF
+-----------------------------------+
  About to ${ACTION} access for...
+-----------------------------------+
  Account: [${ACCOUNT}]          
   Region: [${REGION}]            
      VPC: [${VPCID}]            
+-----------------------------------+

EOF
  read -r -p "${1:-ARE YOU SURE? [y/N]} " response
  case "$response" in
    [yY])
      true
      ;;
    *)
      exit
      ;;
  esac
}

disable_users() {
  echo "Shunning users and disabling accesskeys..."
  for user in $(iam_users); do
    local KEYS=$(accesskeys $user)
    for key in ${KEYS}; do
      aws iam update-access-key --access-key-id $key --status Inactive --user-name $user &&
      echo "- Removed access key [$user : $key]" | tee -a killswitch-${TIMESTAMP}.log
    done
    aws iam delete-login-profile --user-name $user 2> /dev/null && echo "- Removed login profile [$user]" | tee -a killswitch-${TIMESTAMP}.log || echo "- No login profile exists [$user]"
  done
  echo "Shunned all users, except [${CURRENT_USER}], and disabled access (killswitch-${TIMESTAMP}.log)"
}

iam_users() {
  CURRENT_USER=$(aws iam get-user --query "User.UserName" --output text)
  local USERS=(`aws iam list-users --query "Users[*].UserName" --output text | sed -r 's/('"${CURRENT_USER}"'|\t)/ /g'`)
  echo ${USERS[*]}
}

regions() {
  local REGIONS=(`aws ec2 describe-regions --query 'Regions[].{Name:RegionName}' --output text`)
  if [[ ${REGION} == "ALL" ]]; then
    echo ${REGIONS[*]}
  elif [[ ${REGIONS[*]} =~ "${REGION}" && ${REGION} =~ [a-z]{2}-[a-z]+-[0-9] ]]; then
    echo ${REGION}
  else
    echo "ERROR: Invalid region - [${REGION}]"
    exit 1    
  fi
}

shun_vpc() {
  local ACLID=$(aclid $1)
  if [[ -n ${ACLID} ]]; then
    local ENTRIES=$(aws ec2 describe-network-acls \
      --network-acl-ids ${ACLID} \
      --filters "Name=entry.rule-number,Values=1" \
      --query "NetworkAcls[*].Entries[*]" \
      --output table | awk 'NR < 6 || $9~/^1$/; END{print}')
    if [[ -z ${ENTRIES} ]]; then
      aws ec2 create-network-acl-entry --network-acl-id ${ACLID} --rule-number 1 --protocol all --rule-action deny --ingress --cidr-block 0.0.0.0/0 &&
      aws ec2 create-network-acl-entry --network-acl-id ${ACLID} --rule-number 1 --protocol all --rule-action deny --egress --cidr-block 0.0.0.0/0 &&
      echo "Shunned [${1} - (${ACLID})]" | tee -a killswitch-${TIMESTAMP}.log
    else
      echo "Replacing existing ACL entries - [${1} - (${ACLID})]" | tee -a killswitch-${TIMESTAMP}.log
      echo "${ENTRIES}" | tee -a killswitch-${TIMESTAMP}.log
      aws ec2 replace-network-acl-entry --network-acl-id ${ACLID} --rule-number 1 --protocol all --rule-action deny --ingress --cidr-block 0.0.0.0/0 &&
      aws ec2 replace-network-acl-entry --network-acl-id ${ACLID} --rule-number 1 --protocol all --rule-action deny --egress --cidr-block 0.0.0.0/0 &&
      echo "Shunned [${1} - (${ACLID})]" | tee -a killswitch-${TIMESTAMP}.log
    fi
  fi
}

shutdown_ec2() {
  local INSTANCES=$(aws ec2 describe-instances --filters Name=instance-state-name,Values=running --query "Reservations[*].Instances[*].[InstanceId]" --output text)
  echo -e "Shutting down the following EC2 instances:\n${INSTANCES}" | tee -a killswitch-${TIMESTAMP}.log
  aws ec2 stop-instances --instance-ids ${INSTANCES} 2> /dev/null
}

unshun_vpc() {
  local ACLID=$(aclid $1)
  if [[ -n ${ACLID} ]]; then
    local ENTRIES=$(aws ec2 describe-network-acls \
      --network-acl-ids ${ACLID} \
      --filters "Name=entry.rule-number,Values=1" \
      --query "NetworkAcls[*].Entries[*].[RuleNumber,CidrBlock,RuleAction]" \
      --output text | awk '$1~/^1$/ && $2~/0.0.0.0\/0/ && $3~/deny/ {sum += 1} END {print sum}')
    if [[ ${ENTRIES} -gt 0 ]]; then
      aws ec2 delete-network-acl-entry --network-acl-id ${ACLID} --rule-number 1 --ingress &&
      aws ec2 delete-network-acl-entry --network-acl-id ${ACLID} --rule-number 1 --egress &&
      echo "Unshunned [$1 - (${ACLID})]" | tee -a killswitch-${TIMESTAMP}.log
    else
      echo "No previous ACL entries to unshun [$1] " | tee -a killswitch-${TIMESTAMP}.log
    fi
  fi
}

vpcs() {
  if [[ ${VPCID} == "ALL" ]]; then
    echo $(aws ec2 describe-vpcs --query "Vpcs[*].VpcId" --output text)
  else
    echo ${VPCID}
  fi
}

while getopts ":p:r:v:a:h" OPTION; do
  case ${OPTION} in
    p)
      if [[ "${OPTARG:0:1}" == "-" ]]; then
        echo -e "Option -p requires an argument.\n" >&2
        usage
        exit 1
      else
        export AWS_PROFILE=${OPTARG}
      fi
      ;;
    r)
      if [[ "${OPTARG:0:1}" == "-" ]]; then
        echo -e "Option -r requires an argument.\n" >&2
        usage
        exit 1
      else
        REGION=${OPTARG}
      fi
      ;;
    v)
      if [[ "${OPTARG:0:1}" == "-" ]]; then
        echo -e "Option -v requires an argument.\n" >&2
        usage
        exit 1
      else
        VPCID=${OPTARG}
      fi
      ;;
    a)
      if [[ "${OPTARG:0:1}" == "-" ]]; then
        echo -e "Option -a requires an argument.\n" >&2
        usage
        exit 1
      else
        ACTION=${OPTARG}
      fi
      ;;
    h)
      usage
      exit
      ;;
    \?)
      echo -e "Invalid option: -${OPTARG} \n" >&2
      usage
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument." >&2
      usage
      exit 1
      ;;
  esac
done

if [[ -z ${AWS_PROFILE} || -z ${REGION} || -z ${VPCID} || -z ${ACTION} ]]; then
  echo "Required option missing."
  usage
  exit 1
else
  account && regions >> /dev/null && confirm
  echo "$(TZ="UTC" date) - START of ${ACTION} process." >> killswitch-${TIMESTAMP}.log
  case ${ACTION} in
    shun)
      for region in $(regions); do
        export AWS_DEFAULT_REGION=$region
        for vpc in $(vpcs); do
          shun_vpc $vpc
        done
      done
      disable_users
      shutdown_ec2
      ;;
    unshun)
      for region in $(regions); do
        export AWS_DEFAULT_REGION=$region
        for vpc in $(vpcs); do
          unshun_vpc $vpc
        done
      done
      echo "Unshunning users and accesskeys is a manual process, please refer to the shunning output file for affected users."
      ;;
    *)
      echo -e "Invalid action: [${ACTION}] \n" >&2
      usage
      ;;
  esac
  echo "$(TZ="UTC" date) - END of ${ACTION} process." >> killswitch-${TIMESTAMP}.log
fi